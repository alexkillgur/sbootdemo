package com.spdu.springboot.sbd.topic;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

public interface TopicDao {
    List<Topic> getTopics();
    Topic getTopic(int id);
    void addTopic(Topic topic);
    void updateTopic(int id, Topic topic);
    void deleteTopic(int id);
}
